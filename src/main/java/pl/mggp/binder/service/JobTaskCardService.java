/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.service;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mggp.binder.model.dto.TaskByDate;
import pl.mggp.binder.model.dto.TaskByDateDto;
import pl.mggp.binder.repository.JobCardTaskDao;

/**
 *
 * @author wojciech.misiaszek
 */
@Service
public class JobTaskCardService {

    @Autowired
    private JobCardTaskDao jobCardTaskDao;

    public List<TaskByDateDto> getTasksByDateDtos() {
        List<TaskByDateDto> tasksByDateDtos = jobCardTaskDao.getTasksByDateDtos();
        System.out.println(tasksByDateDtos.size());
        return tasksByDateDtos;
    }

    @PostConstruct
    public void test() {

        saveListOfProjectPerUserToCsv(sortTasksToMonthAndYear(getTasksByDateDtos()));

    }

    public Map<LocalDate, List<TaskByDate>> sortTasksToMonthAndYear(List<TaskByDateDto> tasks) {
        Map<LocalDate, List<TaskByDate>> mapOfTaskSortedByMonthAndYear = new TreeMap<>();

        for (TaskByDateDto task : tasks) {
            boolean flag = false;
            LocalDate taskDate = task.getTaskDate();
            LocalDate dateToMap = LocalDate.of(taskDate.getYear(), taskDate.getMonth(), 1);
            TaskByDate taskByDate = new TaskByDate(task);

            if (mapOfTaskSortedByMonthAndYear.containsKey(dateToMap)) {
                List<TaskByDate> tasksByDate = mapOfTaskSortedByMonthAndYear.get(dateToMap);

                for (TaskByDate taskFromList : tasksByDate) {
                    if (taskFromList.equals(taskByDate)) {
                        taskFromList.setTimeSpentOnproject(sumOfTime(taskFromList.getTimeSpentOnproject(), taskByDate.getTimeSpentOnproject()));
                        flag = true;
                    }
                }
                if (!flag) {
                    tasksByDate.add(taskByDate);
                }
            } else {
                List<TaskByDate> listOfTasks = new ArrayList<>();
                listOfTasks.add(taskByDate);
                mapOfTaskSortedByMonthAndYear.put(dateToMap, listOfTasks);
            }
        }

        for (Map.Entry<LocalDate, List<TaskByDate>> entry : mapOfTaskSortedByMonthAndYear.entrySet()) {
            System.out.println(entry.getKey());
            entry.getValue().forEach(System.out::println);
        }
        return mapOfTaskSortedByMonthAndYear;
    }

    public String sumOfTime(String oldTime, String givenTime) {
        String[] hoursAndMinutesOldTime = oldTime.split(":");
        Integer hours = Integer.parseInt(hoursAndMinutesOldTime[0]);
        Integer minutes = Integer.parseInt(hoursAndMinutesOldTime[1]);

        String[] hoursAndMinutesGivenTime = givenTime.split(":");
        hours = Integer.parseInt(hoursAndMinutesGivenTime[0]) + hours;
        minutes = Integer.parseInt(hoursAndMinutesGivenTime[1]) + minutes;
        if (minutes >= 60) {
            minutes = minutes % 60;
            hours++;
        }
        return minutes >= 10 ? String.valueOf(hours) + ":" + String.valueOf(minutes) : String.valueOf(hours) + ":0" + String.valueOf(minutes);
    }

    public void saveListOfProjectPerUserToCsv(Map<LocalDate, List<TaskByDate>> map) {
        try {
            XSSFWorkbook workbook = new XSSFWorkbook();

            for (Map.Entry<LocalDate, List<TaskByDate>> entry : map.entrySet()) {

                String sheetName = entry.getKey().getMonth().toString() + " " + String.valueOf(entry.getKey().getYear());
                XSSFSheet sheet = workbook.createSheet(sheetName);
                XSSFCellStyle style = workbook.createCellStyle();
                style.setAlignment(CellStyle.ALIGN_CENTER);

                int rownum = 1;
                sheet.setColumnWidth(0, 3500);
                sheet.setColumnWidth(1, 11000);
                sheet.setColumnWidth(2, 3500);
                sheet.setColumnWidth(3, 11000);
                sheet.setColumnWidth(4, 2500);
                sheet.setColumnWidth(5, 2500);
                sheet.setDefaultColumnStyle(0, style);
                sheet.setDefaultColumnStyle(1, style);
                sheet.setDefaultColumnStyle(2, style);
                sheet.setDefaultColumnStyle(3, style);
                sheet.setDefaultColumnStyle(4, style);
                sheet.setDefaultColumnStyle(5, style);

                Row rowIitial = sheet.createRow(0);
                Cell cell = rowIitial.createCell(0);
                cell.setCellValue("Numer projektu");

                cell = rowIitial.createCell(1);
                cell.setCellValue("Nazwa projektu");

                cell = rowIitial.createCell(2);
                cell.setCellValue("Numer zadania");

                cell = rowIitial.createCell(3);
                cell.setCellValue("Nazwa zadania");

                cell = rowIitial.createCell(4);
                cell.setCellValue("Ilość godzin");

                cell = rowIitial.createCell(5);
                cell.setCellValue("Ilość minut");

                entry.getValue().sort((o1, o2) -> o1.getNumberOfProject().compareTo(o2.getNumberOfProject()));
                for (TaskByDate taskByDate : entry.getValue()) {
                    Row row = sheet.createRow(rownum++);
                    createList(taskByDate, row);

                }

            }

            try (FileOutputStream out = new FileOutputStream(new File("C:/Projekty/zadaniaWgMiesięcy.xlsx"))) {
                workbook.write(out);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createList(TaskByDate taskByDate, Row row) {
        Cell cell = row.createCell(0);
        cell.setCellValue(taskByDate.getNumberOfProject());

        cell = row.createCell(1);
        cell.setCellValue(taskByDate.getNameOfProject());

        cell = row.createCell(2);
        cell.setCellValue(taskByDate.getNumberOfTask());

        cell = row.createCell(3);
        cell.setCellValue(taskByDate.getNameOfTask());

        String[] time = taskByDate.getTimeSpentOnproject().split(":");
        cell = row.createCell(4);
        cell.setCellValue(time[0]);

        cell = row.createCell(5);
        cell.setCellValue(time[1]);

    }
}
