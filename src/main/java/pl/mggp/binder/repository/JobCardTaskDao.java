/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.mggp.binder.model.dto.TaskByDateDto;
import pl.mggp.binder.model.entity.JobCardTaskEntity;

/**
 *
 * @author wojciech.misiaszek
 */
@Repository
public interface JobCardTaskDao extends JpaRepository<JobCardTaskEntity, Long> {

    @Query("SELECT new pl.mggp.binder.model.dto.TaskByDateDto(p.projectName, p.projectNumber, t.nameOfTask, t.numberOfTask, jc.workTime, jc.dateOfWorkOnTask) "
            + "FROM JobCardTaskEntity jc "
            + "join jc.projectEntity p "
            + "join jc.taskEntity t ")
    public List<TaskByDateDto> getTasksByDateDtos();

}
