/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.configuration;

import javax.annotation.PostConstruct;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * @author wojciech.misiaszek
 */
@Configuration
@ComponentScan("pl.mggp.binder.*")
public class Config implements WebMvcConfigurer{
     
    
    @PostConstruct
    public void printSomething(){
        System.out.println("Spring");
    }
}
