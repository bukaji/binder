/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.model.dto;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author wojciech.misiaszek
 */
public class TaskByDate {

    private String nameOfProject;
    private String numberOfProject;
    private String nameOfTask;
    private String numberOfTask;
    private String timeSpentOnproject;

    public TaskByDate(TaskByDateDto task) {
        this.nameOfProject = task.getNameOfProject();
        this.numberOfProject = task.getNumberOfProject();
        this.nameOfTask = task.getNameOfTask();
        this.numberOfTask = task.getNumberOfTask();
        this.timeSpentOnproject = task.getTimeSpentOnproject().toString();
    }
//    public TaskByDate(String nameOfProject, String numberOfProject, String nameOfTask, String numberOfTask, String timeSpentOnproject) {
//        this.nameOfProject = nameOfProject;
//        this.numberOfProject = numberOfProject;
//        this.nameOfTask = nameOfTask;
//        this.numberOfTask = numberOfTask;
//        this.timeSpentOnproject = timeSpentOnproject;
//    }

    public String getNameOfProject() {
        return nameOfProject;
    }

    public void setNameOfProject(String nameOfProject) {
        this.nameOfProject = nameOfProject;
    }

    public String getNumberOfProject() {
        return numberOfProject;
    }

    public void setNumberOfProject(String numberOfProject) {
        this.numberOfProject = numberOfProject;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    public String getNumberOfTask() {
        return numberOfTask;
    }

    public void setNumberOfTask(String numberOfTask) {
        this.numberOfTask = numberOfTask;
    }

    public String getTimeSpentOnproject() {
        return timeSpentOnproject;
    }

    public void setTimeSpentOnproject(String timeSpentOnproject) {
        this.timeSpentOnproject = timeSpentOnproject;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof TaskByDate) {
            TaskByDate taskByDate = (TaskByDate) obj;
            return this.nameOfProject.equals(taskByDate.nameOfProject)
                    && this.numberOfProject.equals(taskByDate.numberOfProject)
                    && this.nameOfTask.equals(taskByDate.nameOfTask)
                    && this.numberOfTask.equals(taskByDate.numberOfTask);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 17 * nameOfProject.hashCode() + 31 * numberOfProject.hashCode()
                + 7 * nameOfTask.hashCode() + 13 * numberOfTask.hashCode();
    }

    @Override
    public String toString() {
        return "TaskByDate{" + "nameOfProject=" + nameOfProject + ", numberOfProject=" + numberOfProject + ", nameOfTask=" + nameOfTask + ", numberOfTask=" + numberOfTask + ", timeSpentOnproject=" + timeSpentOnproject + '}';
    }

    public static Comparator<TaskByDate> TaskByDateComparator = new Comparator<TaskByDate>() {

        @Override
        public int compare(TaskByDate o1, TaskByDate o2) {
            return o1.getNumberOfProject().compareTo(o2.getNumberOfProject());//To change body of generated methods, choose Tools | Templates.
        }

    };

}
