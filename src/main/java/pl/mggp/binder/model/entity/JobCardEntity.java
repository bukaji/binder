/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author wojciech.misiaszek
 */
@Entity
@Table(name = "job_card")
public class JobCardEntity implements Serializable {

    private Long id;
    private LocalDate jobCardDate;
    private Boolean leave;
    private UserEntity userEntity;

    public JobCardEntity() {
    }

    public JobCardEntity(Long id, LocalDate jobCardDate, Boolean leave, UserEntity userEntity) {
        this.id = id;
        this.jobCardDate = jobCardDate;
        this.leave = leave;
        this.userEntity = userEntity;
    }
    
    

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "job_card_date")
    public LocalDate getJobCardDate() {
        return jobCardDate;
    }

    public void setJobCardDate(LocalDate jobCardDate) {
        this.jobCardDate = jobCardDate;
    }

    public Boolean getLeave() {
        return leave;
    }

    public void setLeave(Boolean leave) {
        this.leave = leave;
    }

    @ManyToOne
    @JoinColumn(name = "created_by")
    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

}
