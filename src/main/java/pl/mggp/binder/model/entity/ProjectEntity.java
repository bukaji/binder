/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author wojciech.misiaszek
 */
@Entity
@Table(name = "project")
public class ProjectEntity implements Serializable{
    
    private Long id;
    private String projectName;
    private String projectNumber;

    public ProjectEntity() {
    }

    public ProjectEntity(Long id, String projectName, String projectNumber) {
        this.id = id;
        this.projectName = projectName;
        this.projectNumber = projectNumber;
    }



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "project_name")
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Column(name = "project_number")
    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }
    
    
    
    
    
}
