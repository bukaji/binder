/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author wojciech.misiaszek
 */
@Entity
@Table(name = "task")
public class TaskEntity implements Serializable {
    
    private Long id;
    private String nameOfTask;
    private String numberOfTask;

    public TaskEntity() {
    }

    public TaskEntity(Long id, String nameOfTask, String numberOfTask) {
        this.id = id;
        this.nameOfTask = nameOfTask;
        this.numberOfTask = numberOfTask;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "task_name")
    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    @Column(name = "task_number")
    public String getNumberOfTask() {
        return numberOfTask;
    }

    public void setNumberOfTask(String numberOfTask) {
        this.numberOfTask = numberOfTask;
    }

    
    
    
    
    
}
